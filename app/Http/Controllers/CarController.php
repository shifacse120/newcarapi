<?php

namespace App\Http\Controllers;
use App\Car;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class CarController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function createCar(Request $request){
        $car    = Car::create($request->all());
        return response()->json($car);
    }
    public function updateCar(Request $request,$id){
        $car = Car::find($id);
        // $car->make  =   $request->get('make');
        // $car->model =   $request->get('model');
        // $car->year  =   $request->get('year');
        // $car->save();
        $car->update($request->all());
       return response()->json($car);
    }
    public function deleteCar($id){
        $car    = Car::find($id);
        $car->delete();
        return response()->json('Removed successfully!');
    }
    public function index(){
        $cars   = Car::all();
        return response()->json($cars);
    }

   
}
